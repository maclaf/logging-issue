package com.makuv.tests;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.servlet.ServletConfig;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;


public class Servlet extends HttpServlet {
    private static final Logger log = LoggerFactory.getLogger(Servlet.class);

    protected static org.apache.commons.logging.Log commonLog = org.apache.commons.logging.LogFactory.getLog(Servlet.class);



    @Override
    public void init(ServletConfig config) throws ServletException {
        log.debug("sth from logger!!");
        commonLog.debug("sth from common loger");
        System.out.println("initialization begin");
        super.init(config);
        System.out.println("initialization end");
    }

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        log.debug("sth from logger22!!");
        System.out.println("sth");
        super.doGet(req, resp);
        System.out.println("sth end");
    }
}